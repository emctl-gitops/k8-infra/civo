# Terraform for Kubernetes Cluster on Civo

## GitOps Demo Group
See [TODO](https://gitlab.com/gitops-demo/readme/-/blob/master/README.md) for the full details.

```
├── backend.tf              # State file Location Configuration
├── k8s-clusters.tf         # Civo Configuration
├── gitlab-gitops-agent.tf  # Adding GitLab kubernetes agent to cluster.
└── vpc.tf                  # AWS VPC Configuration
```
