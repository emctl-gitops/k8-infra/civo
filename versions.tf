terraform {
  required_providers {
    civo = {
      source = "civo/civo"
    }
    gitlab = {
      source = "gitlabhq/gitlab"
    }
    kubernetes = {
      source = "hashicorp/kubernetes"
    }
    helm = {
      source = "hashicorp/helm"
    }
  }
}

# Configure the Civo Provider
provider "civo" {
  token  = var.civo_token
  region = "LON1"
}
